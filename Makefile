main: main.cpp
	g++ -Ofast -Wall -Wextra -Werror main.cpp -o main
run: main
	sh ./run.sh
clean:
	rm main
