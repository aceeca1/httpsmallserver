#include <algorithm>
#include <string>
#include <stdexcept>
#include <unordered_map>
#include <vector>
#include <queue>
#include <random>
#include <cstdio>
#include <climits>
#include <cstring>
#include <strings.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
using namespace std;

constexpr bool CONSOLE_LOG = false;
constexpr bool CONSOLE_PARALLEL = false;

struct Request {
    string url, data;
};

struct CGI {
    void home(string& out, long n = -1) {
        out += "<html>";
        if (n >= 2) {
            out += to_string(n);
            out += " = ";
            bool head = true;
            for (long i = 2; i * i <= n; ++i) {
                while (n % i == 0) {
                    n /= i;
                    if (!head) out += u8"×";
                    head = false;
                    out += to_string(i);
                }
            }
            if (n > 1) {
                if (!head) out += u8"×";
                out += to_string(n);
            }
            out += "<br /><br />";
        }
        out += "<form action='/' method='post'>";
        out += "factorize: ";
        out += "<input name='num' value='";
        static default_random_engine rand;
        static uniform_int_distribution<int> u{2, INT_MAX};
        out += to_string(u(rand));
        out += "'>";
        out += "<input type='submit' value='submit'>";
        out += "</form>";
        out += "</html>";
    }

    void operator()(string& out, Request&& r) {
        if (r.url == "/") {
            if (r.data.empty()) home(out);
            else home(out, strtol(r.data.c_str() + 4, nullptr, 10));
        }
        out +=  "<html> You visited '";
        out +=  r.url;
        out += "'";
        if (!r.data.empty()) {
            out += " with POST data '";
            out += r.data;
            out += "'";
        }
        out +=  ". </html>\r\n";
    }
};

struct Connection {
    static constexpr int PAGE = 4096;
    string in, out;
    int outz = 0, contentLength = -1;
    bool eof = false, noInput = true, inBlock = false, hup = false;
    queue<Request> req;
    Request post;

    Connection(): in(PAGE, 0) {}

    bool readSome(int fd) {
        int z = in.size() - PAGE;
        for (;;) {
            int ret = read(fd, &in[z], PAGE);
            if (ret == 0)  { eof = true; return true; }
            if (ret == -1) return errno & (EAGAIN | EWOULDBLOCK);
            ret += z;
            int s = 0;
            for (;;) if (inBlock) {
                if (ret >= s + contentLength) {
                    z = s + contentLength;
                    if (!readUnit(&in[s], &in[z])) return false;
                    if (!writeNew(fd)) return false;
                    s = z;
                } else break;
            } else {
                auto t = (char*)memchr(&in[z], '\n', ret - z);
                if (!t) break;
                auto tt = t != &in[0] && t[-1] == '\r' ? t - 1 : t;
                if (!readUnit(&in[s], tt)) return false;
                if (!writeNew(fd)) return false;
                if (noInput) { noInput = false; writeSome(fd); }
                s = z = t - &in[0] + 1;
            }
            in.erase(0, s);
            ret -= s;
            in.resize(ret + PAGE);
            z = ret;
        }
    }

    bool writeNew(int fd) {
        if (noInput) {
            noInput = false;
            return writeSome(fd);
        } else return true;
    }

    bool writeSome(int fd) {
        for (;;) {
            if (outz >= (int)out.size()) {
                if (!writeUnit()) return false;
                outz = 0;
                if (out.empty()) { noInput = true; return true; }
            }
            int ret = write(fd, &out[outz], out.size() - outz);
            if (ret == -1) return errno & (EAGAIN | EWOULDBLOCK);
            outz += ret;
        }
    }

    bool readUnit(char* s, char* t) {
        if (CONSOLE_LOG) {
            fprintf(stderr, "\e[1;36m\"%s\"\e[0m\n", string(s, t).c_str());
        }
        if (inBlock) {
            post.data = string(s, t);
            req.emplace(move(post));
            contentLength = -1;
            inBlock = false;
        } else if (equal(s, s + 3, "GET")) {
            auto tt = strchr(s + 4, ' ');
            req.emplace(Request{string(s + 4, tt ? tt : t), {}});
            if (equal(t - 3, t, "1.0")) hup = true;
        } else if (equal(s, s + 4, "POST")) {
            auto tt = strchr(s + 5, ' ');
            post.url = string(s + 5, tt ? tt : t);
            if (equal(t - 3, t, "1.0")) hup = true;
        } else if (equal(s, s + 15, "Content-Length:")) {
            sscanf(s, "%*s%d", &contentLength);
        } else if (s == t && contentLength != -1) {
            inBlock = true;
        }
        return true;
    }

    bool writeUnit() {
        if (req.empty()) {
            if (eof) return false;
            out = "";
            return true;
        }
        out =   "HTTP/1.1 200 OK\r\n";
        out +=  "Content-Type: text/html\r\n";
        out +=  "Content-Length: ";
        int lenStart   = out.size();
        out +=  "2147483647\r\n\r\n";
        int respStart  = out.size();
        CGI()(out, move(req.front()));
        sprintf(&out[lenStart], "%10d", (int)out.size() - respStart);
        out[lenStart + 10] = '\r';
        if (CONSOLE_LOG) fprintf(stderr, "%s", out.c_str());
        req.pop();
        if (hup) eof = true;
        return true;
    }
};

struct Listener {
    static constexpr int MAX_EVENTS = 16;
    int listenDescriptor, epollDescriptor;
    unordered_map<int, Connection> conn;

    Listener(string ip, int port) {
        listenDescriptor = socket(AF_INET, SOCK_STREAM, 0);
        setNonBlock(listenDescriptor);
        int one = 1;
        setsockopt(listenDescriptor,
            SOL_SOCKET, SO_REUSEPORT, &one, sizeof(int));
        if (listenDescriptor == -1) throw runtime_error("socket returns -1");
        sockaddr_in address;
        bzero(&address, sizeof(address));
        address.sin_family = AF_INET;
        inet_aton(ip.c_str(), &address.sin_addr);
        address.sin_port = htons(port);
        sockaddr *addr = (sockaddr*)&address;
        int bindRet = bind(listenDescriptor, addr, sizeof(address));
        if (bindRet == -1) throw runtime_error("bind returns -1");
        listen(listenDescriptor, SOMAXCONN);
        epollDescriptor = epoll_create1(0);
        addEvent(listenDescriptor, EPOLLIN);
    }

    ~Listener() {
        close(epollDescriptor);
        close(listenDescriptor);
    }

    void setNonBlock(int fd) {
        int flags = fcntl(fd, F_GETFL, 0);
        fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    }

    void addEvent(int fd, int events) {
        epoll_event ev;
        ev.events = events;
        ev.data.fd = fd;
        epoll_ctl(epollDescriptor, EPOLL_CTL_ADD, fd, &ev);
    }

    void delEvent(int fd, int events) {
        epoll_event ev;
        ev.events = events;
        ev.data.fd = fd;
        epoll_ctl(epollDescriptor, EPOLL_CTL_DEL, fd, &ev);
    }

    void eventLoop() {
        for (;;) {
            vector<epoll_event> ev(MAX_EVENTS);
            int z = epoll_wait(epollDescriptor, &ev[0], MAX_EVENTS, -1);
            for (int i = 0; i < z; ++i) {
                int fd = ev[i].data.fd, evi = ev[i].events;
                if (fd == listenDescriptor) {
                    acceptOne();
                } else if (evi & EPOLLRDHUP) {
                    dropOne(fd);
                } else if (evi & EPOLLIN) {
                    if (!conn[fd].readSome(fd))   dropOne(fd);
                } else if (evi & EPOLLOUT) {
                    if (!conn[fd].writeSome(fd))  dropOne(fd);
                }
            }
        }
    }

    void acceptOne() {
        int fd = accept(listenDescriptor, nullptr, nullptr);
        if (CONSOLE_PARALLEL) fprintf(stderr,
            "Parallel connections: %d\n", (int)conn.size() + 1);
        if (fd == -1) throw runtime_error("accept returns -1");
        setNonBlock(fd);
        addEvent(fd, EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLET);
    }

    void dropOne(int fd) {
        delEvent(fd, EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLET);
        conn.erase(fd);
        close(fd);
    }
};

int main() {
    Listener li("127.0.0.1", 5555);
    li.eventLoop();
    return 0;
}
