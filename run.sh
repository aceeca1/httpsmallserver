#!/bin/bash
nproc=$(nproc)
while let "nproc > 0"
do
    let "--nproc"
    ./main &
done
